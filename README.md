# bmw-ecu-library

## 1. Data structure
```
- MSx (directory)
  - ShortVin (directory)
    - *_partial.bin // last modified bin file
    - *_partial_flashed.bin // last modified and flashed bin. Readed again to compare *_partial.bin with newly flashed (optional)
    - *_ms41_1429861_partial_stock.bin // stock bin for backup purpose only
    - *_fullread.bin //stock fullread bin for backup purpose only
```

### 2. Ecu list with modifiactions description
## MS41

**AP58395_ms41_1429861**
```
  Engine: m52b25
  Tranmission: manual
  What is done:
    - Popcorn exhaust effect
    - Engine Speed Limiter (Hysterysis A) - Fast cut (szybka odcinka)
```

**BY07540_ms41_1429861** /Ecupro/
```
  Engine: m52b28
  What is done:
    - Engine Speed Limiter (Hysterysis A) - Fast cut (szybka odcinka)
	- Engine RPM Speed Limiter
	- Fuel restore rpm
	- Faster throttle reaction
	- Ews delete
	
	&_v2
	- Popcorn exhaust effect
```

**BU46066_ms41_1432401** /Konrad/
```
  Engine: m52b25
  # v1
  What is done:
	- Engine Speed Limiter (Hysterysis A) - Fast cut (szybka odcinka)
    - Popcorn exhaust effect
	- Engine Speed rpm 7008
	- Ews delete
	- O2 lambda disabled
	
  # v2
  What is done:
	- v1 features
	- O2 enabled
	- DTC enabled
	
  # v3
  What is done:
	- Stock rpm limit
	- v2 features
	
  # v4
  What is done:
	- Engine m52b28
	- v2 features
```

**AP55826_ms41_1429861** /Marchewy/
```
  Engine: m52b28
  # v1
  What is done:
	- Engine Speed Limiter (Hysterysis A) - Fast cut (szybka odcinka)
    - Popcorn exhaust effect
	- Engine Speed rpm
	- Ews delete
	- O2 lambda disabled
	- exhaust flames xD
	- most tuned ecu
	
  # v2
  What is done:
	- O2 lambda enabled
	
  # v3
  What is done:
	- v1 & v2 features
	- Engine: m52b25
	- Without ignition timing modifications (due to engine modifications)
	- stock rpm limit
	  
  # v4
  What is done:
	- v2 features
	- Engine: m52b25
	- with ignition timing modifications and corrected fuel maps modification
```

**BM39429_ms41_1429861**
```
  Engine: m52b20
  Tranmission: auto
  What is done:
    - Ews delete
	- Fuel Restore - RPM
	- Fuel Restroe - RPM (AC on)
	- Ignition retard
	- Fuel Injection - Cranking
	- Ignition timing - Cranking
	- Engine Speed Limiter
```

**BR38392_ms41_1429861**
```
  Engine: m52b28
  What is done: 
	- stock
```

## MS42
** G2 Garage**
```
	 m52b28
	 Engine: m52tub28
	 What is done:
	  - Usunięty Ews
	  - Szybka odcinka
	  - Usunięte sondy
	  - Delikatny popcorn
	  - Usunieta pompa wtórna
```

## MS43
**430056_ms43**
```
	Virgin
```

**G2 Garage**
```
	Wszystkie wsady:
	  - Usunięty Ews
	  - Szybka odcinka
	  - Usunięte sondy
	  - Delikatny popcorn
	  - Usunieta pompa wtórna
```

# Zasady dodawania nowych plików do repozytorium
* Nowe pliki dodajemy na branch develop
* Pliki umieszczami w ustalonej strukturze katalogów ** 1. Data structure**
* Uzupełniamy listę plików w pkt. **2. Ecu list with modifications description** zawierający podstawowe dane przesłanego 
wsadu. Preferowany język angielski z polskim tłumaczeniem ale nie wymagany.
* Po uzupełnieniu własnej paczki z plikami i opisem readme wypychamy wszystko do brancha **develop** oraz wykonujemy **Pull Request** 
do 
brancha **master**. 
a) W polu z wyborem źródłowego brancha w lewej kolumnie wybierz `develop`, a kolumnie prawej `master`
b) W polu **reviewers** wpisz `texaar` aby dodać mnie do recenzentów.
W ten sposób utrzymujemy porządek w repozytorium tak by wszystkie pliki były jasno i czytelnie opisane dla każdego 
kto z niego skorzysta.
